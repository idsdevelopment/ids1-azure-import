﻿using System;
using System.Threading.Tasks;
using Ids;
using Utils;

namespace Ids1Import.Azure
{
	public partial class AzureClient : IdsClient
	{
		public void SignOut()
		{
			Task.Run( () =>
			{
				using( var Client = new AzureClient() )
					Client.RequestSignOut().Wait();
			} );
		}

		public bool HasTimeError { get; set; }

		public ( bool InError, bool IsTimeError) LogIn( string carrierId, string userName, string password )
		{
			var RetVal = ( InError: true, IsTimeError: false );

			var AToken = RequestLogin( Encryption.ToTimeLimitedToken( carrierId ), Encryption.ToTimeLimitedToken( userName ), Encryption.ToTimeLimitedToken( password ), DateTimeOffset.Now.Offset.Hours.ToString(), "Version test",
									   DebugServer, Encryption.ToTimeLimitedToken( DateTime.UtcNow.Ticks.ToString() ) ).Result;

			if( !string.IsNullOrWhiteSpace( AToken ) )
			{
				if( AToken != TIME_ERROR )
				{
					AuthToken = AToken;
					RetVal.InError = false;
				}
				else
					RetVal.IsTimeError = HasTimeError = true;
			}

			return RetVal;
		}
	}
}