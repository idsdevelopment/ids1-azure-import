﻿using System;
using Ids;

namespace Ids1Import.Azure
{
	public partial class AzureClient : IdsClient
	{
		private const string PRODUCTION_END_POINT = "http://idsroute.azurewebsites.net/",
							 ALPHAT_END_POINT = "https://idsroute-alphat.azurewebsites.net",
							 ALPHAC_END_POINT = "https://idsroute-alphac.azurewebsites.net",
							 BETA_END_POINT = "https://idsroute-beta.azurewebsites.net",
							 GAMMA_END_POINT = "https://idsroute-gamma.azurewebsites.net";

		public const string TIME_ERROR = "TimeError";

		public static bool _DebugServer = false;

		public enum SLOT
		{
			PRODUCTION,
			ALPHAT,
			ALPHAC,
			BETA,
			GAMMA
		}

	#if ALPHA_T
        public static SLOT Slot = SLOT.ALPHAT;
#else
		public static SLOT Slot = SLOT.PRODUCTION;
	#endif

		private static readonly object LockObject = new object();
		public static string _AuthToken = "";

		public static string AuthToken
		{
			set
			{
				if( string.IsNullOrEmpty( value ) )
					throw new ArgumentException( "Auth token set to empty" );
				lock( LockObject )
					_AuthToken = value;
			}
			get
			{
				lock( LockObject )
					return _AuthToken;
			}
		}

		public AzureClient( string endPoint ) : base( endPoint, AuthToken )
		{
		}

		public static string GetSlot()
		{
			switch( Slot )
			{
			case SLOT.ALPHAT:
				return ALPHAT_END_POINT;
			case SLOT.ALPHAC:
				return ALPHAC_END_POINT;
			case SLOT.BETA:
				return BETA_END_POINT;
			case SLOT.GAMMA:
				return GAMMA_END_POINT;
			default:
				return PRODUCTION_END_POINT;
			}
		}

		public AzureClient() : this( GetSlot() )
		{
		}

		public string DebugServer => _DebugServer ? "t" : "f";
	}
}