﻿using System.IO;
using Microsoft.Azure.WebJobs;

namespace Ids1Import
{
	public class Functions
	{
		// This function will get triggered/executed when a new message is written 
		// on an Azure Queue called queue.
		public static void ProcessQueueMessage( [ QueueTrigger( "queue" ) ] string message, TextWriter log )
		{
			log.WriteLine( message );
		}

		public static void Import()
		{
			using( new Import.Import( Program.SourceAccount, Program.DestinationAccount, Program.UserName, Program.Password ) )
			{
			}
		}

		// Runs once every 5 minutes
		public static void CronJob( [ TimerTrigger( "0 */5 * * * *" ) ] TimerInfo timer )
		{
			Import();
		}
	}
}