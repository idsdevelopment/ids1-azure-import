﻿using System.Linq;
using Ids1Import.Azure;

namespace Ids1Import
{
	// To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
	internal class Program
	{
		private const string SOURCE_ACCOUNT_ID = "/SAC=",
							 DESTINATION_ACCOUNT = "/DAC=",
							 SLOT = "/SLOT=",
							 USER_ID = "/U=",
							 PASSWORD_ID = "/P=";

		// Slots
		private const string ALPHAT = "ALPHAT",
							 ALPHAC = "ALPHAC",
							 BETA = "BETA",
							 GAMMA = "GAMMA";

		internal static string SourceAccount, DestinationAccount, Slot, UserName, Password;

		// Please set the following connection strings in app.config for this WebJob to run:
		// AzureWebJobsDashboard and AzureWebJobsStorage
		private static void Main( string[] args )
		{
			foreach( var A in args.Select( arg => arg.Trim() ) )
			{
				if( A.StartsWith( SOURCE_ACCOUNT_ID ) )
					SourceAccount = A.Remove( 0, SOURCE_ACCOUNT_ID.Length ).Trim();

				else if( A.StartsWith( DESTINATION_ACCOUNT ) )
					DestinationAccount = A.Remove( 0, DESTINATION_ACCOUNT.Length ).Trim();

				else if( A.StartsWith( USER_ID ) )
					UserName = A.Remove( 0, USER_ID.Length ).Trim();

				else if( A.StartsWith( PASSWORD_ID ) )
					Password = A.Remove( 0, PASSWORD_ID.Length ).Trim();

				else
				{
					switch( Slot = A.Remove( 0, SLOT.Length ).Trim().ToUpper() )
					{
					case ALPHAT:
						AzureClient.Slot = AzureClient.SLOT.ALPHAT;
						break;
					case ALPHAC:
						AzureClient.Slot = AzureClient.SLOT.ALPHAC;
						break;
					case BETA:
						AzureClient.Slot = AzureClient.SLOT.BETA;
						break;
					case GAMMA:
						AzureClient.Slot = AzureClient.SLOT.GAMMA;
						break;

					default:
						AzureClient.Slot = AzureClient.SLOT.PRODUCTION;
						break;
					}
				}
			}

			if( string.IsNullOrEmpty( DestinationAccount ) )
				DestinationAccount = SourceAccount;

		#if !DEBUG
            var Config = new JobHostConfiguration();

			if( Config.IsDevelopment )
				Config.UseDevelopmentSettings();

			Config.UseTimers();
			Config.Queues.MaxDequeueCount = 2;
			Config.Queues.BatchSize = 1;

			var Host = new JobHost( Config );

			// The following code ensures that the WebJob will be running continuously
			Host.RunAndBlock();
#else
			Functions.Import();
		#endif
		}
	}
}