﻿using System;
using System.Linq;
using System.Net;
using Ids1Import.Azure;
using Ids1Import.Database;
using Ids1Import.MySql;

namespace Ids1Import.Import
{
	public class Import : IDisposable
	{
		private const string SSH_HOST =  "139.99.148.60",
                             SSH_USER_NAME = "root",
							 SSH_PASSWORD = "11dsadm!N",
							 DATABASE_HOST =  "139.99.186.49",
							 DATABASE = "ids";

		private const int SSH_PORT = 22,
						  REMOTE_MYSQL_PORT = 3306,
						  LOCAL_MYSQL_PORT = 14324;

		private const int COMMAND_TIMEOUT_IN_SECONDS = 600;
		private const int TRIP_BATCH_COUNT = 10000;

		private readonly MySqlSsh MySqlSsh;

		public Import( string sourceAccount, string destinationAccount, string userName, string password )
		{
			ServicePointManager.DefaultConnectionLimit = 64;

			MySqlSsh = new MySqlSsh( SSH_HOST, SSH_PORT, DATABASE_HOST, REMOTE_MYSQL_PORT, LOCAL_MYSQL_PORT, SSH_USER_NAME, SSH_PASSWORD, DATABASE );
			if( MySqlSsh.Connected )
			{
				using( var Client = new AzureClient() )
				{
					var LoginOk = !Client.LogIn( destinationAccount, userName, password ).InError;
					/*
					if( LoginOk )
					{
						var Settings = Client.RequestImportSettings().Result;

						using( var Db = new idsEntities( MySqlSsh.EntityConnection ) )
						{
							var D = Db;

							var Accnts = from Account in D.accounts
										 where ( Account.mainId == sourceAccount ) && ( Account.lastUpdated > Settings.LastAccountUpdate )
										 let Address = ( from Addr in D.addresses
														 where Addr.internalId == Account.address_internalId
														 select Addr ).FirstOrDefault()
										 select new { Account, Address };

							long MaxUpdate = 0;

							foreach( var Accnt in Accnts )
							{
								if( Accnt.Account.lastUpdated != null )
									MaxUpdate = Math.Max( MaxUpdate, (long)Accnt.Account.lastUpdated );
							}
						}

						var SkipCount = 0;
						var TotalTrips = 0;
						while( true )
						{
							using( var Db = new idsEntities( MySqlSsh.EntityConnection ) )
							{
								Db.Database.CommandTimeout = COMMAND_TIMEOUT_IN_SECONDS;

								var Trips = ( from Trip in Db.trips
											  where ( Trip.mainId == sourceAccount ) && ( Trip.lastUpdated > Settings.LastTripUpdate )
											  let PickupAddress = ( from A in Db.addresses
																	where A.internalId == Trip.pickupAddress_internalId
																	select A ).FirstOrDefault()
											  let DeliveryAddress = ( from A in Db.addresses
																	  where A.internalId == Trip.deliveryAddress_internalId
																	  select A ).FirstOrDefault()
											  orderby Trip.lastUpdated
											  select new { Trip, PickupAddress, DeliveryAddress } ).Skip( SkipCount ).Take( TRIP_BATCH_COUNT ).ToList();

								if( Trips.Count == 0 )
									break;

								TotalTrips += Trips.Count;
								SkipCount += TRIP_BATCH_COUNT;
							}
						}
					}
					*/
				}
			}
		}

		private void ReleaseUnmanagedResources()
		{
			// TODO release unmanaged resources here
		}

		protected virtual void Dispose( bool disposing )
		{
			ReleaseUnmanagedResources();
			if( disposing )
				MySqlSsh?.Dispose();
		}

		public void Dispose()
		{
			Dispose( true );
			GC.SuppressFinalize( this );
		}

		~Import()
		{
			Dispose( false );
		}
	}
}