﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using Renci.SshNet;

namespace Ids1Import.Ssh
{
	public class SshConnection : IDisposable
	{
		private readonly SshClient Client;
		private readonly ForwardedPortLocal PortFwld;
		public bool Connected { get; }

		internal SshConnection( string sshHost, int sshPort,  string remoteHost, int remotePort, int localPort, string userName, string password, int connectionTimeoutInSeconds = 30 )
		{
			try
			{
				var Pauth = new PasswordAuthenticationMethod( userName, password );
				var Kauth = new KeyboardInteractiveAuthenticationMethod( userName );
				Kauth.AuthenticationPrompt += ( sender, args ) =>
				{
					foreach( var Prompt in args.Prompts.Where( prompt => prompt.Request.IndexOf( "Password", StringComparison.InvariantCultureIgnoreCase ) != -1 ) )
					{
						Prompt.Response = password;
						break;
					}
				};

				var ConnectionInfo = new ConnectionInfo(sshHost, sshPort, userName, Pauth, Kauth ) { Timeout = TimeSpan.FromSeconds( connectionTimeoutInSeconds ) };
				Client = new SshClient( ConnectionInfo );
				Client.Connect();

				if( Client.IsConnected )
				{
					Debug.WriteLine( "Connected" );
					PortFwld = new ForwardedPortLocal( IPAddress.Loopback.ToString(), (uint)localPort, remoteHost, (uint)remotePort );
					Client.AddForwardedPort( PortFwld );
					PortFwld.Start();
					Connected = PortFwld.IsStarted;

					Debug.WriteLine( Connected ? "Port bound" : "Port NOT bound" );
				}
				else
					Debug.WriteLine( "NOT Connected" );
			}
			catch( Exception Exception )
			{
				Debug.WriteLine( Exception );
				throw;
			}
		}

		public bool IsBound => ( Client != null ) && Client.IsConnected && ( PortFwld != null ) && PortFwld.IsStarted;
		public string BoundHost => PortFwld != null ? PortFwld.BoundHost : "";
		public int BoundPort => PortFwld != null ? (int)PortFwld.BoundPort : -1;

		private void ReleaseUnmanagedResources()
		{
			// TODO release unmanaged resources here
		}

		protected virtual void Dispose( bool disposing )
		{
			ReleaseUnmanagedResources();
			if( disposing )
			{
				Client?.Dispose();
				PortFwld?.Dispose();
			}
		}

		public void Dispose()
		{
			Dispose( true );
			GC.SuppressFinalize( this );
		}

		~SshConnection()
		{
			Dispose( false );
		}
	}
}