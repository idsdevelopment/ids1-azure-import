﻿using System.Data.Common;
using System.Data.Entity;

namespace Ids1Import.Database
{
	// ReSharper disable once InconsistentNaming
	public partial class idsEntities : DbContext
	{
		public idsEntities( DbConnection connection ) : base( connection, false )
		{
		}
	}
}