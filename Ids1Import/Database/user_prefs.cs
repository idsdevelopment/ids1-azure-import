//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ids1Import.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class user_prefs
    {
        public string internalId { get; set; }
        public string prefId { get; set; }
        public bool prefBoolean { get; set; }
        public long prefLong { get; set; }
        public double prefDouble { get; set; }
        public byte[] pref { get; set; }
        public string user_internalId { get; set; }
        public string prefString { get; set; }
    }
}
