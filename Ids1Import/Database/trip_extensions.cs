//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ids1Import.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class trip_extensions
    {
        public string internalId { get; set; }
        public string resellerId { get; set; }
        public string accountId { get; set; }
        public string tripId { get; set; }
        public string trip_internalId { get; set; }
        public Nullable<int> multipleIndex { get; set; }
        public string valueId { get; set; }
        public Nullable<bool> valueBoolean { get; set; }
        public Nullable<long> valueLong { get; set; }
        public Nullable<double> valueDouble { get; set; }
        public string valueString { get; set; }
        public Nullable<System.DateTime> valueTimestamp { get; set; }
        public byte[] value { get; set; }
        public Nullable<System.DateTime> createDate { get; set; }
        public string tripInternalId { get; set; }
    
        public virtual trip trip { get; set; }
    }
}
